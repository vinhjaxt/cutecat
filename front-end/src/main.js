import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import mixins from './mixins'
import './registerServiceWorker'

Vue.config.productionTip = false

Vue.mixin(mixins)

window.app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
