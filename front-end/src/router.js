import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue'),
      meta: {
        title: 'Thú cưng dễ thương ^^'
      }
    },
    {
      path: '/upload',
      name: 'upload',
      component: () => import('./views/Upload.vue'),
      meta: {
        title: 'Tải lên hình ảnh'
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = (to.meta && to.meta.title ? to.meta.title : 'My Website') + ' | ' + location.hostname
  next()
})

export default router
