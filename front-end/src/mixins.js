import ifetch from 'ifetch-browser'
import { API_URL } from '@/config'
export default {
  methods: {
    ifetch: ifetch.defaults({
      json: true,
      url: API_URL
    }),
    onScrollLoadMore (e, childElement, fn, fullyInViewOrInWindow) {
      const parentElement = e.target
      if (!childElement) return

      const docElem = document.documentElement
      const docScrollTop = (window.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0)
      const docHeight = docElem.clientHeight
      const parentTop = isNaN(parentElement.scrollTop) ? docScrollTop : parentElement.scrollTop
      const parentBottom = parentTop + ((isNaN(parentElement.clientHeight) ? docHeight : parentElement.clientHeight) || 0)

      let box = {
        top: 0,
        left: 0
      }
      // If we don't have gBCR, just use 0,0 rather than error
      // BlackBerry 5, iOS 3 (original iPhone)
      if (typeof childElement.getBoundingClientRect !== 'undefined') {
        box = childElement.getBoundingClientRect()
      }
      const elementTop = box.top + (window.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0)
      const elementBottom = elementTop + childElement.clientHeight

      let isIntoView = false
      if (fullyInViewOrInWindow === true) {
        isIntoView = ((parentTop < elementTop) && (parentBottom > elementBottom))
      } else {
        isIntoView = ((elementTop <= parentBottom) && (elementBottom >= parentTop))
      }

      if (isIntoView && childElement.oldTop) {
        if (childElement.oldTop < elementTop) {
          // scroll up
          console.log('scroll up:', childElement.oldTop, elementTop, childElement, parentElement)
          fn('up', true, parentElement)
        } else {
          // scroll down
          console.log('scroll down:', childElement.oldTop, elementTop, childElement, parentElement)
          fn('down', false, parentElement)
        }
      }
      childElement.oldTop = elementTop
    },
    debounce (a, b, c) {
      console.log('debounce:', a.name)
      var d
      return function () {
        var e = this
        var f = arguments
        return new Promise(function (resolve, reject) {
          try {
            clearTimeout(d)
            d = setTimeout(function () {
              d = null
              if (!c) {
                resolve(a.apply(e, f))
              }
            }, b)
            if (c && !d) {
              resolve(a.apply(e, f))
            }
          } catch (e) {
            reject(e)
          }
        })
      }
    },
    error () {
      console.error('error', ...arguments)
    },
    log () {
      console.log('log', ...arguments)
    }
  }
}
