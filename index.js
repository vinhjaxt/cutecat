const path = require('path')
const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const mysql = require('mysql')
const crypto = require('crypto')
const fs = require('fs')

function md5 (t) {
  return crypto.createHash('md5').update(t).digest('hex')
}

const pathWritable = '/var/tmp/uploads'
if (!fs.existsSync(pathWritable)) {
  fs.mkdir(pathWritable, e => {
    if (e) {
      console.error(e)
    }
  })
}

const db = mysql.createPool({
  host: process.env.DATABASE_SERVICE_NAME,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME
})
db.on('connection', conn => {
  conn.query('set NAMES utf8')
})

const multer = require('multer')
const upload = multer({
  limits: {
    fileSize: 2 * 1024 * 1024 // 2MB
  },
  storage: multer.diskStorage({
    destination: pathWritable,
    filename: function (req, file, cb) {
      cb(null, path.basename(file.originalname) + '-' + md5(file.originalname + file.size) + file.extLower)
    }
  }),
  fileFilter (req, file, cb) {
    const filetypes = /jpe?g|png|gif/
    const mimetype = filetypes.test(file.mimetype)
    const extLower = path.extname(file.originalname).toLowerCase()
    file.extLower = extLower
    const extname = filetypes.test(extLower)
    if (mimetype && extname) {
      return cb(null, true)
    }
    // eslint-disable-next-line standard/no-callback-literal
    cb('Error: File upload only supports the following filetypes - ' + filetypes)
  }
}).single('file')

app.disable('x-powered-by')
app.use((req, res, next) => {
  if (req.headers['origin']) {
    // console.log(req.method.toUpperCase()) // GET, POST, OPTIONS
    res.append('Access-Control-Allow-Origin', req.headers['origin'] || '*')
    // res.append('Access-Control-Allow-Origin', '*')
    // res.append('Access-Control-Allow-Origin', 'null');
    res.append('Access-Control-Allow-Credentials', 'true')
    res.append('Access-Control-Max-Age', 3600 * 2)
  }
  if (['HEAD', 'OPTIONS'].includes(req.method.toUpperCase()) || req.headers['access-control-request-method']) {
    // res.append('Allow', 'GET, POST, OPTIONS');
    // res.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    res.append('Access-Control-Allow-Methods', req.headers['access-control-request-method'] || req.method.toUpperCase())
    res.append('Access-Control-Allow-Headers', req.headers['access-control-request-headers'] || 'Origin, Content-Type, Authorization, Content-Length, X-Requested-With, Accept, X-Token, x-token')
    res.append('Accept', 'application/json, text/javascript, text/plain, */*')
    if (['HEAD', 'OPTIONS'].includes(req.method.toUpperCase())) {
      res.end()
      return
    }
  }
  next()
})
app.use(require('compression')())
app.use('/', express.static('./front-end/dist'))
app.use('/uploads', express.static(pathWritable))

app.get('/api/imgs', (req, res) => {
  let limit = isNaN(req.query.limit) ? 20 : req.query.limit
  if (limit > 50) limit = 50
  const lastID = isNaN(req.query.start) ? 0 : req.query.start
  db.query('select id, src from posts where id > ? limit ' + limit, [lastID], (e, r) => {
    if (e) {
      console.error(e)
      res.json({
        error: 'db error'
      })
      return
    }
    res.json(r)
  })
})

app.post('/api/upload', (req, res) => {
  upload(req, res, e => {
    if (e instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      console.error(e)
      res.json({ error: 'Upload error' })
      return
    } else if (e) {
      // An unknown error occurred when uploading.
      console.error(e)
      res.json({ error: 'unknown error' })
      return
    }
    // Everything went fine.
    const src = 'uploads/' + req.file.filename
    db.query('insert posts (src, creation_time) values (?, ?)', [src, Math.floor(Date.now() / 1000)], (e, r) => {
      if (e) {
        console.error(e)
        return
      }
      io.emit('new-post', { id: r.insertId, src })
    })
    res.json({ success: true })
  })
})

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something bad happened!')
})

server.listen((process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080), (process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'), () => console.log('App running..'))
